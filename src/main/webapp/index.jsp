<%@ page import="java.time.LocalDate" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Credit calculator</title>
</head>
<body>
<h1>Credit calculator</h1>

<form action="index.jsp" method="post">
    Kwota kredytu <input type="number" name="loanAmount">
    Oprocentowanie:<input type="number" name="interestRate">
    Liczba rat <input type="number" name="installments" step="1">
    <input type="submit" value="Oblicz">
</form>

<jsp:useBean id="loan" class="pl.sdacademy.Loan"/>
<%--Poniższa linia ustawia wszystkie właściwości bean-a--%>
<jsp:setProperty name="loan" property="*"/>

Wysokość raty:
<jsp:getProperty name="loan" property="installmentAmount"/>

<%
    LocalDate now = LocalDate.now();
    for (int i=0; i<loan.getInstallments(); i++) {
        LocalDate installmentDate = now.plusMonths(i);
        out.print(installmentDate + " " + loan.getInstallmentAmount() + "<br>");
    }
%>
</body>
</html>
